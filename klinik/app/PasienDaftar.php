<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasienDaftar extends Model
{
    protected $table = 'pasiendaftar';
    protected $primaryKey = 'nopendaftaran';
    public $incrementing = false;
    protected $fillable = [
        'id',
    	'nopendaftaran',
        'idpasien',
        'nama',
        'status',
    ];

    public function pasien() {
    	return $this->belongsto('App\Pasien');
	}
}
 