@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Pasien</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group">
						<label>ID Pasien</label>
						<input type="text" name="id" id="id" class="form-control" readonly="" value="{{ $pasien['id'] }}">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group">
						<label>No Pendaftaran</label>
						<input type="text" name="nopendaftaran" id="nopendaftaran" class="form-control" readonly="" value="{{ $nopendaftaran}}">
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">	
					<div class="form-group">
						<label>Nama Pasien</label>
						<input type="text" name="nama" id="nama" class="form-control" readonly="" value="{{ $pasien['nama'] }}">
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div class="form-group">
						<label>Umur Pasien</label>
						<input type="text" name="umur" readonly="" class="form-control" value="{{$umur->y}} Tahun">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<a class="btn btn-success" data-toggle="modal" href='#modal-riwayat'>Lihat riwayat rekam medis</a>
				</div>
			</div>
		</div>

		{{-- modal riwayat rekam medis --}}
					<div class="modal fade" id="modal-riwayat">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Riwayat Rekam Medis</h4>
								</div>
								<div class="modal-body">
									<table class="table table-striped table-bordered">
										<tr>
											<thead>
												<tr>
													<th width="15%">Tanggal Periksa</th>
													<th>Hasil Rekam Medis</th>
												</tr>
											</thead>
											<tbody>
												@if($rekamMedis)
												@foreach($rekamMedis as $data)
												<tr>
													<td>{{ date('d-m-Y', strtotime($data['created_at'])) }}</td>
													<td>
														Keluhan: <strong>{{$data['keluhan']}} </strong>|
														Anamnesis: <strong>{{$data['anamnesis']}} </strong>|
														Diagnosa: <strong>{{$data['diagnosa']}} </strong>|
														Tindakan: <strong>{{$data['tindakan']}} </strong>|
														Keterangan: <strong>{{$data['keterangan']}} </strong>
													</td>
												</tr>
												@endforeach
												@else
												<tr>
													<td>-</td>
													<td>Tidak ada data rekam medis</td>
												</tr>
												@endif
											</tbody>
										</tr>
									</table>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>

		<div class="x_panel">
			<div class="x_title">
				<h2>PEMERIKSAAN FISIK PASIEN <span class="badge" style="background: #1e88e5;color: #ffffff">{{ $pasien['id'] }}</span></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					<li><a class="close-link"><i class="fa fa-close"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<form  method="post" id="frm-rekam-medis">
						<input type="hidden" name="rk_medis" id="rk_medis" value="{{$id}}">
						<input type="hidden" name="nama" id="nama" value="{{$nama}}">
						<input type="hidden" name="tgl_lahir" id="tgl_lahir" value="{{$tgl_lahir}}">
						<input type="hidden" name="dokter_id" id="dokter_id" value="{{ Session::get('id') }}">
						<input type="hidden" name="pasien_id" id="pasien_id" value="{{ $pasien['id'] }}">

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label>Kesadaran</label>
								<input type="text" name="kesadaran" class="form-control" id="kesadaran" >
							</div>
						</div>

						<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<div class="form-group">
								<label>Tekanan Darah</label>
								<div class="input-group">
									<input type="number" class="form-control" name="tensi" id="tensi"  >
									<div class="input-group-addon">mmHg</div>
								</div>
							</div>
						</div>

						<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<div class="form-group">
								<label>Nadi</label>
								<div class="input-group">
									<input type="number" class="form-control" name="nadi" id="nadi"  >
									<div class="input-group-addon">x/mnt</div>
								</div>
							</div>
						</div>

						<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<div class="form-group">
								<label>Suhu</label>
								<div class="input-group">
									<input type="number" class="form-control" name="suhu" id="suhu"  >
									<div class="input-group-addon">&deg;C</div>
								</div>
							</div>
						</div>

						<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<div class="form-group">
								<label>Pernapasan</label>
								<div class="input-group">
									<input type="number" class="form-control" name="pernapasan" id="pernapasan"  >
									<div class="input-group-addon">x/mnt</div>
								</div>
							</div>
						</div>

						<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<div class="form-group">
								<label>Tinggi Badan</label>
								<div class="input-group">
									<input type="number" class="form-control" name="tb" id="tb" >
									<div class="input-group-addon">Cm</div>
								</div>
							</div>
						</div>

						<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<div class="form-group">
								<label>Berat Badan</label>
								<div class="input-group">
									<input type="number" class="form-control" name="bb" id="bb" >
									<div class="input-group-addon">Kg</div>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label>Kesadaran Umum</label>
								<div class="form-group">
									<div style="float: left;display: block">
										<div class="radio">
											<label>
												<input type="radio" name="kesadaran" id="baik" value="baik"  checked="checked">
												Baik / 
											</label>
											<label>
												<input type="radio" name="kesadaran" id="sedang" value="sedang">
												Sedang / 
											</label>
											<label>
												<input type="radio" name="kesadaran" id="buruk" value="buruk">
												Buruk
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label>Pemeriksaan Fisik</label>
								<textarea class="form-control" id="pemeriksaan" name="pemeriksaan" rows="4" ></textarea>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label>Daftar Masalah</label>
								<textarea class="form-control" id="daftarmasalah" name="daftarmasalah" rows="4" ></textarea>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label>Pengkajian Masalah</label>
								<textarea class="form-control" id="pengkajian" name="pengkajian" rows="4" ></textarea>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label>Diet</label>
								<textarea class="form-control" id="diet" name="diet" rows="4" ></textarea>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label>Terapi/Instruksi Medis</label>
								<textarea class="form-control" id="terapi" name="terapi" rows="4" ></textarea>
							</div>
						</div>
					</div>
				</div>

				
			</div>

			<div class="x_footer">
					<button class="btn btn-md btn-flat btn-primary btn-block" type="submit">
					Simpan <i class="fa fa-save"></i>
					</button>
			</div>
		</div>
	</form>
</div>
@endsection
@section('customJs')
<script type="text/javascript">
	$(document).ready(function() {
		// alergi obat
		$('#ya').click(function() {
			$('#alergi_obat').prop('disabled', false);
		});
		$('#tidak').click(function() {
			$('#alergi_obat').prop('disabled', true);
		});
		// isi obat
		$('.btn-tambah').on('click', function(event) {
			event.preventDefault();
			var no = $('.no').length;
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			$.ajax({
			url: "/dokter/getObat",
			type: 'GET',
			dataType: 'JSON',
			success: function (data) {
				var obat = '<div class="form-group no"><label>Obat '+(no+1)+'</label><div class="row"><div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"><select class="form-control select2 obat"  name="obat[]"><option disabled="" selected="">- Pilih -</option></select></div><div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"><input type="text" name="jumlah[]" placeholder="jumlah" class="form-control" ></div><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top:5px"><input type="text" placeholder="keterangan" name="keterangan[]" class="form-control"></div></div></div>';
				$('#daftar-obat').append(obat);
				$.each(data, function(i, item){
					$('.obat').append($("<option/>", {
								value: item.id,
								text: item.nama
								}));
						});
						$('.select2').select2();
					}
					});
					
				});
				$('#frm-rekam-medis').on('submit', function(e) {
					e.preventDefault();
					var id = $('#rk_medis').val();
					var pasien_id = $('#pasien_id').val();
					var nama = $('#nama').val();
					var tgl = $('#tgl_lahir').val();
					var dokter_id = $('#dokter_id').val();
					
					var bb = $('#bb').val();
					var tensi = $('#tensi').val();
					var tb = $('#tb').val();
					var suhu = $('#suhu').val();
					var nadi = $('#nadi').val();
					var pernapasan = $('#pernapasan').val();
					var kesadaran = $('#kesadaran').val();
					var pemeriksaan = $('#pemeriksaan').val();
					var daftarmasalah = $('#daftarmasalah').val();
					var pengkajian = $('#pengkajian').val();
					var diet = $('#diet').val();
					var terapi = $('#terapi').val();
					var nopendaftaran = $('#nopendaftaran').val();
		
					if ($('#tidak').val()) {
						var alergi = $('#tidak').val();
					}else {
						var alergi = $('#alergi_obat').val();
					}
					var obat = $('select[name="obat[]"]').serializeArray();
					var jumlah = $('input[name="jumlah[]"]').serializeArray();
					var keterangan = $('input[name="keterangan[]"]').serializeArray();
					$.post("{{route('postRekamMedisPasien')}}", {
						id:id,
						pasien_id:pasien_id,
						nama:nama,
						tgl:tgl,
						dokter_id:dokter_id,
						bb:bb,
						tensi:tensi,
						tb:tb,
						suhu:suhu,
						nadi:nadi,
						pernapasan:pernapasan,
						kesadaran:kesadaran,
						pemeriksaan:pemeriksaan,
						daftarmasalah:daftarmasalah,
						pengkajian:pengkajian,
						diet:diet,
						terapi:terapi,
						nopendaftaran:nopendaftaran,

						obat:obat,
						jumlah:jumlah,
						keterangan:keterangan,
					}, function(data) {
						console.log(data);
						toastr.success('Success !', 'Data berhasil di simpan !');
						location.href = "{{route('dokter.index')}}";
					});
				});
			});
	</script>
	@endsection