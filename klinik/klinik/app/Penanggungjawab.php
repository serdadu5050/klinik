<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penanggungjawab extends Model
{ 
    protected $table = 'penanggungjawab';
    protected $fillable = [
    	'idpasien',
        'nama_penanggung_jawab',
        'hubungan_keluarga',
        'alamat_penanggung_jawab',
        'pekerjaan_penanggun_jawab',
        'telp_penanggung_jawab',
	
    ];

   
}
