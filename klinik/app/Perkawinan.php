<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perkawinan extends Model
{
    protected $table = 'perkawinan';
    protected $fillable = [
    	'perkawinan'
    ];
    
}
