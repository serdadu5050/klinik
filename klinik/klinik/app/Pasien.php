<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $table = 'pasiens';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'id',
    	'nama',
    	'jenis_kelamin',
        'alamat',
        'tempatlahir',
        'tgl_lahir',
        'telp',
        'nohp',
        'perkawinan',
        'agama',
        'pendidikan',
        'noktp',
        'jaminan',
        'hobi',
        'pekerjaan',
        'namakantor',
        'golongandarah',
        'catatan',
        'status',
        'layanan_dokter'
    ];
}
  