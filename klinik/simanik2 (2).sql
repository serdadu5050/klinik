-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 07, 2018 at 07:19 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simanik2`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `level`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'admin', '$2y$10$2jWB3JBzBZuxWiaxte70c.w1K/hoSvqgq7Dvquyj.2dMBHx7CV5zG', 'admin', '2018-09-19 04:24:24', '2018-09-19 04:24:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `agama`
--

CREATE TABLE `agama` (
  `id` int(10) UNSIGNED NOT NULL,
  `agama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agama`
--

INSERT INTO `agama` (`id`, `agama`, `created_at`, `updated_at`) VALUES
(1, 'Islam', '2018-09-19 08:46:21', '2018-09-19 08:46:25'),
(2, 'Kristen', NULL, NULL),
(3, 'Katolik', NULL, NULL),
(4, 'Budha', NULL, NULL),
(5, 'Hindu', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `apotekers`
--

CREATE TABLE `apotekers` (
  `id` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` date NOT NULL,
  `level` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dokters`
--

CREATE TABLE `dokters` (
  `id` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` date NOT NULL,
  `spesialis_id` smallint(6) NOT NULL,
  `level` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dokters`
--

INSERT INTO `dokters` (`id`, `username`, `password`, `nama`, `alamat`, `tgl_lahir`, `spesialis_id`, `level`, `photo`, `created_at`, `updated_at`, `remember_token`) VALUES
('DK001', 'dr', '$2y$10$t.Dc9v5th/ycB90qDoiZJeixNNTsrKQauzSCtbXLUThoXk0N.bS5e', 'Dr.boyke', 'jakarta', '2018-09-06', 1, 'dokter', 'user-dokter.jpg', '2018-09-19 08:53:19', '2018-09-19 08:53:19', NULL),
('DK002', 'agus', '$2y$10$IW7QRZQGM8hdiXehAdLDseppQ8p/WAGoJwxwqB8qOJf3ocneumGG6', 'agus', 'aa', '2018-11-27', 1, 'dokter', 'user-dokter.jpg', '2018-11-28 06:24:41', '2018-11-28 06:24:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `golongandarah`
--

CREATE TABLE `golongandarah` (
  `id` int(10) UNSIGNED NOT NULL,
  `golongandarah` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `golongandarah`
--

INSERT INTO `golongandarah` (`id`, `golongandarah`, `created_at`, `updated_at`) VALUES
(1, 'A', '2018-11-15 06:47:08', '2018-11-15 06:47:08'),
(2, 'B', '2018-11-15 06:47:08', '2018-11-15 06:47:08'),
(3, 'AB', '2018-11-15 06:47:08', '2018-11-15 06:47:08'),
(4, 'O', '2018-11-15 06:47:08', '2018-11-15 06:47:08'),
(5, 'A +', '2018-11-15 06:47:08', '2018-11-15 06:47:08'),
(6, 'A -', '2018-11-15 06:47:08', '2018-11-15 06:47:08'),
(7, 'B +', '2018-11-15 06:47:08', '2018-11-15 06:47:08'),
(8, 'B -', '2018-11-15 06:47:08', '2018-11-15 06:47:08'),
(9, 'AB +', '2018-11-15 06:47:08', '2018-11-15 06:47:08'),
(10, 'AB -', '2018-11-15 06:47:08', '2018-11-15 06:47:08'),
(11, 'O +', '2018-11-15 06:47:08', '2018-11-15 06:47:08'),
(12, 'O -', '2018-11-15 06:47:08', '2018-11-15 06:47:08');

-- --------------------------------------------------------

--
-- Table structure for table `hubkeluarga`
--

CREATE TABLE `hubkeluarga` (
  `id` int(10) UNSIGNED NOT NULL,
  `hubkeluaraga` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hubkeluarga`
--

INSERT INTO `hubkeluarga` (`id`, `hubkeluaraga`, `created_at`, `updated_at`) VALUES
(1, 'Buruh', '2018-09-19 08:46:21', '2018-09-19 08:46:25');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_obat`
--

CREATE TABLE `kategori_obat` (
  `id` int(10) UNSIGNED NOT NULL,
  `kategori` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori_obat`
--

INSERT INTO `kategori_obat` (`id`, `kategori`, `created_at`, `updated_at`) VALUES
(1, 'generik', NULL, NULL),
(2, 'non generik', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(31, '2017_08_01_075024_create_dokters_table', 1),
(32, '2017_08_01_075106_create_apotekers_table', 1),
(33, '2017_08_01_075255_create_resepsionists_table', 1),
(34, '2017_08_01_075332_create_pasiens_table', 1),
(35, '2017_08_01_075413_create_rk_medis_table', 1),
(36, '2017_08_01_075513_create_reseps_table', 1),
(37, '2017_08_01_075541_create_obats_table', 1),
(38, '2017_08_02_032344_create_admins_table', 1),
(39, '2017_08_04_101707_create_spesialis_table', 1),
(40, '2017_08_10_113316_create_kategori_obat_table', 1),
(41, '2018_09_25_114128_create_penjamin_table', 2),
(42, '2018_09_25_114356_create_golongandarah_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `obats`
--

CREATE TABLE `obats` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kandungan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `harga` double(8,2) NOT NULL,
  `status` enum('ada','habis') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `obats`
--

INSERT INTO `obats` (`id`, `nama`, `kandungan`, `kategori_id`, `harga`, `status`, `created_at`, `updated_at`) VALUES
(1, 'paracetamol', '200', 1, 5000.00, 'ada', NULL, NULL),
(2, '', '', 0, 0.00, 'ada', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pasiendaftar`
--

CREATE TABLE `pasiendaftar` (
  `id` int(10) UNSIGNED NOT NULL,
  `nopendaftaran` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idpasien` char(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pasiendaftar`
--

INSERT INTO `pasiendaftar` (`id`, `nopendaftaran`, `idpasien`, `nama`, `status`, `created_at`, `updated_at`) VALUES
(4, 'REG00001', 'PS0001', 'Aan Fauzan', 'antri', '2018-09-28 06:55:16', '2018-09-28 06:55:16'),
(5, 'REG00002', 'PS0002', 'jaka', 'antri', '2018-09-28 07:05:02', '2018-09-28 07:05:02'),
(6, 'REG00003', 'PS0003', 'alal', 'antri', '2018-09-28 07:12:35', '2018-09-28 07:12:35'),
(7, 'REG00004', 'PS0004', 'asdm', 'antri', '2018-09-28 07:14:33', '2018-09-28 07:14:33'),
(8, 'REG00005', 'PS0005', 'nmaksdmk', 'antri', '2018-09-28 07:16:29', '2018-09-28 07:16:29'),
(9, 'REG00006', 'PS0006', 'test 6', 'antri', '2018-09-28 10:22:26', '2018-09-28 10:22:26'),
(10, 'REG00007', 'PS0007', 'dedi', 'antri', '2018-10-01 08:26:23', '2018-10-01 08:26:23'),
(11, 'REG00008', 'PS0008', 'rico', 'antri', '2018-10-01 08:29:51', '2018-10-01 08:29:51'),
(12, 'REG00009', 'PS0001', 'Aan Fauzan', 'antri', '2018-10-01 09:43:42', '2018-10-01 09:43:42'),
(13, 'REG00010', 'PS0003', 'alal', 'antri', '2018-10-01 09:48:14', '2018-10-01 09:48:14'),
(14, 'REG00011', 'PS0005', 'nmaksdmk', 'antri', '2018-10-01 09:48:39', '2018-10-01 09:48:39'),
(15, 'REG00012', 'PS0009', 'ajag', 'antri', '2018-10-02 06:41:14', '2018-10-02 06:41:14'),
(16, 'REG00013', 'PS0009', 'ajag', 'antri', '2018-10-04 09:26:20', '2018-10-04 09:26:20'),
(17, 'REG00014', 'PS0009', 'ajag', 'antri', '2018-10-04 09:26:21', '2018-10-04 09:26:21'),
(18, 'REG00015', 'PS0007', 'dedi', 'obat', '2018-10-05 03:17:55', '2018-10-05 03:56:56'),
(19, 'REG00016', 'PS0010', 'yudi', 'antri', '2018-10-10 10:40:12', '2018-10-10 10:40:12'),
(20, 'REG00017', 'PS0010', 'yudi', 'antri', '2018-10-31 08:34:39', '2018-10-31 08:34:39'),
(21, 'REG00018', 'PS0010', 'yudi', 'antri', '2018-10-31 08:34:40', '2018-10-31 08:34:40'),
(22, 'REG00019', 'PS0010', 'yudi', 'antri', '2018-10-31 08:34:40', '2018-10-31 08:34:40'),
(23, 'REG00020', 'PS0010', 'yudi', 'antri', '2018-10-31 08:34:40', '2018-10-31 08:34:40'),
(24, 'REG00021', 'PS0010', 'yudi', 'antri', '2018-10-31 08:34:40', '2018-10-31 08:34:40'),
(25, 'REG00022', 'PS0010', 'yudi', 'antri', '2018-10-31 08:34:40', '2018-10-31 08:34:40'),
(26, 'REG00023', 'PS0010', 'yudi', 'antri', '2018-10-31 08:34:40', '2018-10-31 08:34:40'),
(27, 'REG00024', 'PS0010', 'yudi', 'antri', '2018-10-31 08:34:41', '2018-10-31 08:34:41'),
(28, 'REG00025', 'PS0010', 'yudi', 'antri', '2018-10-31 08:34:41', '2018-10-31 08:34:41'),
(29, 'REG00026', 'PS0010', 'yudi', 'antri', '2018-10-31 08:34:41', '2018-10-31 08:34:41'),
(30, 'REG00027', 'PS0002', 'jaka', 'antri', '2018-11-09 07:52:32', '2018-11-09 07:52:32'),
(31, 'REG00028', 'PS0003', 'alal', 'antri', '2018-11-09 07:54:06', '2018-11-09 07:54:06'),
(32, 'REG00029', 'PS0009', 'ajag', 'antri', '2018-11-23 07:03:51', '2018-11-23 07:03:51'),
(33, 'REG00030', 'PS0007', 'dedi', 'antri', '2018-11-23 07:03:55', '2018-11-23 07:03:55'),
(34, 'REG00031', 'PS0006', 'test 6', 'antri', '2018-11-23 07:03:58', '2018-11-23 07:03:58'),
(35, 'REG00032', 'PS0011', 'aan', 'antri', '2018-11-28 04:32:15', '2018-11-28 04:32:15'),
(36, 'REG00033', 'PS0011', 'dedy', 'antri', '2018-12-03 06:35:17', '2018-12-03 06:35:17'),
(37, 'REG00034', 'PS0012', 'dedya', 'antri', '2018-12-03 06:42:19', '2018-12-03 06:42:19'),
(38, 'REG00035', 'PS0018', 'dedyaq', 'antri', '2018-12-03 08:07:44', '2018-12-03 08:07:44');

-- --------------------------------------------------------

--
-- Table structure for table `pasiens`
--

CREATE TABLE `pasiens` (
  `id` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` enum('pria','wanita') COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempatlahir` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` date NOT NULL,
  `telp` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nohp` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `perkawinan` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agama` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pendidikan` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noktp` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jaminan` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hobi` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pekerjaan` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `namakantor` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `golongandarah` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catatan` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `layanan_dokter` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pasiens`
--

INSERT INTO `pasiens` (`id`, `nama`, `jenis_kelamin`, `alamat`, `tempatlahir`, `tgl_lahir`, `telp`, `nohp`, `perkawinan`, `agama`, `pendidikan`, `noktp`, `jaminan`, `hobi`, `pekerjaan`, `namakantor`, `golongandarah`, `catatan`, `status`, `layanan_dokter`, `created_at`, `updated_at`) VALUES
('PS0001', 'Aan Fauzan', 'pria', 'jakarta', 'jakarta', '2018-09-05', '012312', '', '1', '1', '3', '', '2', '', '1', 'imu', '2', 'test', 'obat', '', '2018-09-28 06:55:16', '2018-09-28 10:35:48'),
('PS0002', 'jaka', 'pria', 'jln baru', 'bandung', '2018-09-05', '01231231', '', '1', '1', '3', '', '1', 'volly', '1', 'imu', '2', 'teuing', 'obat', '', '2018-09-28 07:05:02', '2018-09-28 10:51:40'),
('PS0003', 'alal', 'pria', 'jln,asih', 'jakarta', '2018-09-02', '098981293812', '', '1', '1', '1', '01823818283712', '1', 'men ball', '1', 'imu', '2', 'teuing', 'antri', '', '2018-09-28 07:12:35', '2018-09-28 07:12:35'),
('PS0004', 'asdm', 'pria', 'kasdas', 'jakarta', '2018-08-29', '09213123', '', '1', '1', '2', '0034', '2', 'bola', '1', 'pt argo', '2', 'mkasmdkjkjaksjdk', 'antri', '', '2018-09-28 07:14:33', '2018-09-28 07:14:33'),
('PS0005', 'nmaksdmk', 'pria', 'jaksnkdank', 'amnskdnaks', '2018-09-19', '9182903801', '', '2', '2', '2', '123912839', '1', 'kanskdnaks', '1', 'NJASNDJKNKA', '3', 'AJSNDKJANSJKN', 'antri', '', '2018-09-28 07:16:29', '2018-09-28 07:16:29'),
('PS0006', 'test 6', 'pria', 'kjaksjdkjaskd', 'jakarta', '2018-09-10', '0919238912', '', '3', '4', '2', '0090993843', '3', 'maen ball', '1', 'jakarta', '3', 'kjaskdkas', 'antri', '', '2018-09-28 10:22:26', '2018-09-28 10:22:26'),
('PS0007', 'dedi', 'pria', 'cijantung', 'jakarta', '2018-11-23', '019230912', '', '3', '3', '2', '9898797987', '3', 'asdas', '1', 'kasmndkamn', '3', 'masmdka', 'antri', '', '2018-10-01 08:26:22', '2018-11-23 07:02:34'),
('PS0009', 'ajag', 'pria', 'kkkkkkkkkkkkkkk', 'jakarta', '2018-11-23', '019230912', '', '3', '3', '2', '099182', '3', 'asdas', '1', 'kasmndkamn', '3', 'masmdka', 'antri', '', '2018-10-02 06:41:14', '2018-11-23 07:03:00'),
('PS0010', 'yudi', 'pria', 'zzzzzzzzzzzzzzzz', 'jakarta', '2018-11-23', '019230912', '', '3', '3', '2', '0921938912839182', '3', 'asdas', '1', 'kasmndkamn', '3', 'masmdka', 'antri', '', '2018-10-10 10:40:12', '2018-11-23 07:03:15'),
('PS0011', 'dedy', 'pria', 'jakarta', 'jakarta', '2018-12-02', '00000', '', '1', '1', '1', '11111', '1', 'main', '1', 'main', '2', 'ddd', 'antri', '', '2018-12-03 06:35:17', '2018-12-03 06:35:17'),
('PS0012', 'dedya', 'wanita', 'ja', 'jakarta', '2018-12-02', '11111', '', '2', '1', '', '31313', '1', 'main', '1', 'main', '2', 'ff', 'antri', '', '2018-12-03 06:42:18', '2018-12-03 06:42:18'),
('PS0013', 'dedyaaad', 'pria', 'daada', 'jakarta', '2018-12-04', '101010', '', '3', '2', '2', '111111', '1', 'main', '1', 'main', '2', 'sas', 'antri', '', '2018-12-03 06:54:11', '2018-12-03 06:54:11'),
('PS0014', 'dedyaaads', 'pria', 'ds', 'jakarta', '2018-12-02', '111111', '', '3', '4', '4', '3131311', '1', 'ds', '1', 'main', '5', 'jakarta', 'antri', '', '2018-12-03 07:04:10', '2018-12-03 07:04:10'),
('PS0015', 'dedyaaadx', 'pria', 'x', 'jakarta', '2018-11-13', '111111', '', '4', '2', '3', '11111111', '3', 'main', '1', 'main', '2', 'daad', 'antri', '', '2018-12-03 07:08:34', '2018-12-03 07:08:34'),
('PS0016', 'dedyaaadx', 'wanita', 'ssa', 'jakarta', '2018-12-02', '1111111', '', '3', '4', '2', '111111111', '3', 'main', '1', 'main', '2', 'aaaa', 'antri', '', '2018-12-03 07:31:03', '2018-12-03 07:31:03'),
('PS0017', 'aass', 'wanita', 'asa', 'jakarta', '2018-12-02', '00000', '', '3', '4', '3', '111111111', '1', 'ds', '1', 'main', '3', 'jakarta', 'antri', '', '2018-12-03 07:56:45', '2018-12-03 07:56:45'),
('PS0018', 'dedyaq', 'pria', 'haa', 'jakarta', '2018-12-03', '1111111', '', '2', '2', '2', '11111111', '3', 'main', '1', 'jaka', '3', 'dada', 'antri', '', '2018-12-03 08:07:44', '2018-12-03 08:07:44');

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan`
--

CREATE TABLE `pekerjaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `pekerjaan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pekerjaan`
--

INSERT INTO `pekerjaan` (`id`, `pekerjaan`, `created_at`, `updated_at`) VALUES
(1, 'Buruh', '2018-09-19 08:46:21', '2018-09-19 08:46:25');

-- --------------------------------------------------------

--
-- Table structure for table `penanggungjawab`
--

CREATE TABLE `penanggungjawab` (
  `id` int(10) UNSIGNED NOT NULL,
  `idpasien` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_penanggung_jawab` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hubungan_keluarga` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_penanggung_jawab` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pekerjaan_penanggun_jawab` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telp_penanggung_jawab` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tempatlahir_penanggung_jawab` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir_penanggung_jawab` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `penanggungjawab`
--

INSERT INTO `penanggungjawab` (`id`, `idpasien`, `nama_penanggung_jawab`, `hubungan_keluarga`, `alamat_penanggung_jawab`, `pekerjaan_penanggun_jawab`, `telp_penanggung_jawab`, `created_at`, `updated_at`, `tempatlahir_penanggung_jawab`, `tgl_lahir_penanggung_jawab`) VALUES
(12, 'PS0001', 'deni', 'suami', 'jln baru', '1', '02132321', '2018-09-28 06:55:16', '2018-09-28 06:55:16', '', '0000-00-00'),
(13, 'PS0002', 'dini', 'istri', 'jln', '1', '0213123', '2018-09-28 07:05:02', '2018-09-28 07:05:02', '', '0000-00-00'),
(14, 'PS0003', 'jono', 'istri', 'jln,baru', '1', '021312312', '2018-09-28 07:12:35', '2018-09-28 07:12:35', '', '0000-00-00'),
(15, 'PS0004', 'jajsdnjas', 'suami', 'kjnasjdnajsd', '1', '09090', '2018-09-28 07:14:33', '2018-09-28 07:14:33', '', '0000-00-00'),
(16, 'PS0005', 'JAKARTA', 'orangtua', 'JNASJKDNJKASNJK', '1', '87198237981278', '2018-09-28 07:16:29', '2018-09-28 07:16:29', '', '0000-00-00'),
(17, 'PS0006', 'kaskdmkasd', 'istri', 'kasmdkamsk', '1', '123123', '2018-09-28 10:22:26', '2018-09-28 10:22:26', '', '0000-00-00'),
(18, 'PS0007', 'gvvvhv', 'suami', 'vgvghvgh', '1', 'hjbhjvhjvhj', '2018-10-01 08:26:22', '2018-10-01 08:26:22', '', '0000-00-00'),
(19, 'PS0008', 'asd', 'suami', 'asdasd', '1', '001239012', '2018-10-01 08:29:51', '2018-10-01 08:29:51', '', '0000-00-00'),
(20, 'PS0009', 'najksndjkansjk', 'orangtua', 'askdnklasmnd', '1', '9081092830', '2018-10-02 06:41:14', '2018-10-02 06:41:14', '', '0000-00-00'),
(21, 'PS0010', 'asjkdnakjsndjkanj', 'suami', 'naskjdnajksndjk', '1', 'jknasjkdnakjsn', '2018-10-10 10:40:12', '2018-10-10 10:40:12', '', '0000-00-00'),
(22, 'PS0011', 'agus', 'orangtua', 'jakarta', '1', '0000', '2018-11-28 04:32:15', '2018-11-28 04:32:15', '', '0000-00-00'),
(23, 'PS0011', 'agus', 'orangtua', 'jakarta', '1', '000000', '2018-12-03 06:35:17', '2018-12-03 06:35:17', '', '0000-00-00'),
(24, 'PS0012', 'ad', 'suami', 'aa', '1', '0000', '2018-12-03 06:42:18', '2018-12-03 06:42:18', '', '0000-00-00'),
(25, 'PS0018', 'agusaa', 'orangtua', 'ada', '1', '00000001', '2018-12-03 08:07:44', '2018-12-03 08:07:44', 'jakarta', '2018-12-02');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE `pendidikan` (
  `id` int(10) UNSIGNED NOT NULL,
  `pendidikan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`id`, `pendidikan`, `created_at`, `updated_at`) VALUES
(1, 'TK', '2018-09-19 08:46:21', '2018-09-19 08:46:25'),
(2, 'SD', NULL, NULL),
(3, 'SMP', NULL, NULL),
(4, 'SMA', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pengirim`
--

CREATE TABLE `pengirim` (
  `id` int(10) UNSIGNED NOT NULL,
  `idpasien` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_pengirim` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instansi_pengirim` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `diagnosa_pengirim` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_datang` datetime(6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengirim`
--

INSERT INTO `pengirim` (`id`, `idpasien`, `nama_pengirim`, `instansi_pengirim`, `diagnosa_pengirim`, `tgl_datang`, `created_at`, `updated_at`) VALUES
(10, 'PS0001', 'jaka', 'swasta', 'batuk', '2018-09-28 00:00:00.000000', '2018-09-28 06:55:16', '2018-09-28 06:55:16'),
(11, 'PS0002', 'jack', 'pemerintah', 'batuk', '2018-09-28 00:00:00.000000', '2018-09-28 07:05:02', '2018-09-28 07:05:02'),
(12, 'PS0003', 'indo', 'swasta', 'batuk', '2018-09-28 00:00:00.000000', '2018-09-28 07:12:35', '2018-09-28 07:12:35'),
(13, 'PS0004', 'nasndjansjd', 'pemerintah', 'batuk', '2018-09-28 00:00:00.000000', '2018-09-28 07:14:33', '2018-09-28 07:14:33'),
(14, 'PS0005', 'JASNDJKNAJK', 'swasta', 'meriang', '2018-09-28 00:00:00.000000', '2018-09-28 07:16:29', '2018-09-28 07:16:29'),
(15, 'PS0006', 'asdasd', 'pemerintah', 'batuk', '2018-09-06 00:00:00.000000', '2018-09-28 10:22:26', '2018-09-28 10:22:26'),
(16, 'PS0007', 'xdfxdfxfd', 'pemerintah', 'pilek', NULL, '2018-10-01 08:26:22', '2018-10-01 08:26:22'),
(17, 'PS0008', 'asdasd', 'pemerintah', 'batuk', NULL, '2018-10-01 08:29:51', '2018-10-01 08:29:51'),
(18, 'PS0009', 'nasdnankjas', 'pemerintah', 'pilek', '2018-10-17 00:00:00.000000', '2018-10-02 06:41:14', '2018-10-02 06:41:14'),
(19, 'PS0010', 'asnmdjklnajskn', 'lainnya', 'batuk', NULL, '2018-10-10 10:40:12', '2018-10-10 10:40:12'),
(20, 'PS0011', 'dedi', 'pemerintah', 'batuk', '2018-11-28 00:00:00.000000', '2018-11-28 04:32:15', '2018-11-28 04:32:15'),
(21, 'PS0011', 'dedi', 'pemerintah', 'batuk', '2018-12-03 00:00:00.000000', '2018-12-03 06:35:17', '2018-12-03 06:35:17'),
(22, 'PS0012', 'dedi', 'pemerintah', 'pilek', '2018-12-04 00:00:00.000000', '2018-12-03 06:42:19', '2018-12-03 06:42:19'),
(23, 'PS0018', 'dediq', 'pemerintah', 'pilek', '2018-12-03 00:00:00.000000', '2018-12-03 08:07:44', '2018-12-03 08:07:44');

-- --------------------------------------------------------

--
-- Table structure for table `penjamin`
--

CREATE TABLE `penjamin` (
  `id` int(10) UNSIGNED NOT NULL,
  `penjamin` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `penjamin`
--

INSERT INTO `penjamin` (`id`, `penjamin`, `created_at`, `updated_at`) VALUES
(1, 'UMUM', NULL, NULL),
(2, 'BPJS', NULL, NULL),
(3, 'Perusahaan', NULL, NULL),
(4, 'Private Insurance', NULL, NULL),
(5, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `perkawinan`
--

CREATE TABLE `perkawinan` (
  `id` int(10) UNSIGNED NOT NULL,
  `perkawinan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `perkawinan`
--

INSERT INTO `perkawinan` (`id`, `perkawinan`, `created_at`, `updated_at`) VALUES
(1, 'Kawin', '2018-09-19 08:46:21', '2018-09-19 08:46:25'),
(2, 'Belum Kawin', '2018-09-19 09:21:20', '2018-09-19 09:21:24'),
(3, 'Duda', '2018-09-19 09:21:26', '2018-09-19 09:21:28'),
(4, 'Janda', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reseps`
--

CREATE TABLE `reseps` (
  `id` int(10) UNSIGNED NOT NULL,
  `dokter_id` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `obat_id` smallint(6) NOT NULL,
  `pasien_id` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` smallint(6) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reseps`
--

INSERT INTO `reseps` (`id`, `dokter_id`, `obat_id`, `pasien_id`, `keterangan`, `jumlah`, `status`, `created_at`, `updated_at`) VALUES
(1, 'DK001', 1, 'PS0003', '3x1', 10, 'belum', '2018-09-25 10:49:39', '2018-09-25 10:49:39'),
(2, 'DK001', 1, 'PS0001', '2x1', 100, 'belum', '2018-09-28 10:35:48', '2018-09-28 10:35:48'),
(3, 'DK001', 1, 'PS0002', 'asd', 90, 'belum', '2018-09-28 10:51:40', '2018-09-28 10:51:40'),
(4, 'DK001', 1, 'PS0002', 'asd', 90, 'belum', '2018-09-28 10:53:30', '2018-09-28 10:53:30');

-- --------------------------------------------------------

--
-- Table structure for table `resepsionists`
--

CREATE TABLE `resepsionists` (
  `id` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` date NOT NULL,
  `level` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resepsionists`
--

INSERT INTO `resepsionists` (`id`, `username`, `password`, `nama`, `alamat`, `tgl_lahir`, `level`, `photo`, `created_at`, `updated_at`, `remember_token`) VALUES
('RS001', 'aan', '$2y$10$pNQ/10UXBCbCheMAO/6YFejD09mknrKOBdsVSTKxS3e5U0fL9eNMy', 'aan', 'jln banda', '0000-00-00', 'resepsionist', '2018-09-19.foto-wallpapers.jpeg', '2018-09-19 04:26:10', '2018-09-19 04:26:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rk_medis`
--

CREATE TABLE `rk_medis` (
  `id` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nopendaftaran` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pasien_id` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_lahir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokter_id` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diagnosa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keluhan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anamnesis` text COLLATE utf8mb4_unicode_ci,
  `tindakan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `alergi_obat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bb` double(8,2) NOT NULL,
  `tb` double(8,2) NOT NULL,
  `tensi` double(8,2) NOT NULL,
  `bw` enum('ya','tidak') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suhu` double(8,2) DEFAULT NULL,
  `nadi` double(8,2) DEFAULT NULL,
  `pernapasan` double(8,2) DEFAULT NULL,
  `kesadaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pemeriksaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `daftarmasalah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pengkajian` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `diet` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terapi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rk_medis`
--

INSERT INTO `rk_medis` (`id`, `nopendaftaran`, `pasien_id`, `nama`, `tgl_lahir`, `dokter_id`, `diagnosa`, `keluhan`, `anamnesis`, `tindakan`, `keterangan`, `alergi_obat`, `bb`, `tb`, `tensi`, `bw`, `suhu`, `nadi`, `pernapasan`, `kesadaran`, `pemeriksaan`, `daftarmasalah`, `pengkajian`, `diet`, `terapi`, `created_at`, `updated_at`) VALUES
('RK0001', 'REG00001', 'PS0001', 'Aan Fauzan', '2018-09-05', 'DK001', 'asd,a s,', 'sakit', 'asmdkam', 'sa,d,a', 'asjdnjansdj', 'tidak', 123.00, 213.00, 50.00, 'tidak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-28 10:35:48', '2018-09-28 10:35:48'),
('RK0002', 'REG00002', 'PS0002', 'jaka', '2018-09-05', 'DK001', 'asda', 'sada', 'sada', 'sdfs', 'sdfsd', 'tidak', 123.00, 434.00, 23.00, 'tidak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-28 10:59:15', '2018-09-28 10:59:15'),
('RK0004', 'REG00013', 'PS0009', 'ajag', '2018-10-15', 'DK001', NULL, NULL, NULL, NULL, NULL, NULL, 66.00, 55.00, 111.00, NULL, 33.00, 22.00, 44.00, 'jiuqhjuwe', 'jansdj', 'nandskllnmak', 'iwqjiejqi', 'njsandjan', 'asndioan', '2018-10-02 09:37:53', '2018-10-02 09:37:53'),
('RK0005', 'REG00014', 'PS0009', 'ajag', '2018-10-15', 'DK001', NULL, NULL, NULL, NULL, NULL, NULL, 7654.00, 135.00, 5426.00, NULL, 57475.00, 5.00, 6334.00, 'Tidak Sadar', NULL, NULL, NULL, NULL, NULL, '2018-10-04 09:27:47', '2018-10-04 09:27:47'),
('RK0007', 'REG00003', 'PS0007', 'dedi', '2018-09-30', 'DK001', NULL, NULL, NULL, NULL, NULL, NULL, 999.00, 555.00, 121.00, NULL, 444.00, 222.00, 333.00, 'kmaskdm', 'asmdkam', 'masdkmdkl', 'kasmdklam', 'aklsmkdma', 'akasmdkl', '2018-10-05 03:56:56', '2018-10-05 03:56:56');

-- --------------------------------------------------------

--
-- Table structure for table `spesialis`
--

CREATE TABLE `spesialis` (
  `id` int(10) UNSIGNED NOT NULL,
  `spesialis` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `spesialis`
--

INSERT INTO `spesialis` (`id`, `spesialis`, `created_at`, `updated_at`) VALUES
(1, 'Kelamin', '2018-09-19 08:52:51', '2018-09-19 08:52:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agama`
--
ALTER TABLE `agama`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apotekers`
--
ALTER TABLE `apotekers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dokters`
--
ALTER TABLE `dokters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `golongandarah`
--
ALTER TABLE `golongandarah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hubkeluarga`
--
ALTER TABLE `hubkeluarga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_obat`
--
ALTER TABLE `kategori_obat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `obats`
--
ALTER TABLE `obats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pasiendaftar`
--
ALTER TABLE `pasiendaftar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pasiens`
--
ALTER TABLE `pasiens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penanggungjawab`
--
ALTER TABLE `penanggungjawab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengirim`
--
ALTER TABLE `pengirim`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjamin`
--
ALTER TABLE `penjamin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perkawinan`
--
ALTER TABLE `perkawinan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reseps`
--
ALTER TABLE `reseps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resepsionists`
--
ALTER TABLE `resepsionists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rk_medis`
--
ALTER TABLE `rk_medis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spesialis`
--
ALTER TABLE `spesialis`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `agama`
--
ALTER TABLE `agama`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `golongandarah`
--
ALTER TABLE `golongandarah`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `hubkeluarga`
--
ALTER TABLE `hubkeluarga`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kategori_obat`
--
ALTER TABLE `kategori_obat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `obats`
--
ALTER TABLE `obats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pasiendaftar`
--
ALTER TABLE `pasiendaftar`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `penanggungjawab`
--
ALTER TABLE `penanggungjawab`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `pendidikan`
--
ALTER TABLE `pendidikan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pengirim`
--
ALTER TABLE `pengirim`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `penjamin`
--
ALTER TABLE `penjamin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `perkawinan`
--
ALTER TABLE `perkawinan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reseps`
--
ALTER TABLE `reseps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `spesialis`
--
ALTER TABLE `spesialis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
