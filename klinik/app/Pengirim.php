<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengirim extends Model
{ 
    protected $table = 'pengirim';
    protected $fillable = [
    	'idpasien',
        'nama_pengirim',
        'instansi_pengirim',
        'diagnosa_pengirim',
        'tgl_datang',
    ];

   
}
