@extends('layouts.app') @section('content')
<!-- top tiles -->
<div class="row tile_count">
  <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total Pasien</span>
    <div class="count">{{ count($total) }}</div>
  </div>
  <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-clock-o"></i> Pasien Bulan Ini</span>
    <div class="count">{{ count($bulan) }}</div>
  </div>
  <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Pasien Hari Ini</span>
    <div class="count green">{{ count($pasien) }}</div>
  </div>
</div>
<hr>
<!-- /top tiles -->

<div class="col-lg-12">
  <div class="page-title">
    <div class="title_left">
      <h3><i class="fa fa-users"></i> Pedaftaran Pasien</h3>
    </div>
  </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
  <div class="x_panel">

    <div class="x_title">
      <h2>Data Pasien</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>

    <div class="x_content">
      <form action="{{ route('postPendaftaranPasien') }}" method="post" id="frm-pasien">
        {{ csrf_field() }}
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label>ID</label>
            <input type="text" name="id" class="form-control" required="" value="{{$id}}" >
            <input type="hidden" name="status" value="antri">
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label>No.KTP/SIM</label>
            <input type="text" name="noktp" class="form-control">
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control" required="">
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label>Tempat Lahir</label>
            <input type="text" name="tempatlahir" class="form-control">
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label>Tanggal Lahir</label>
            <div class="input-group">
              <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
              <input type="text" name="tgl_lahir" class="form-control datepicker">
            </div>
          </div>
        </div>

        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label>Alamat</label>
            <textarea class="form-control" name="alamat" required=""></textarea>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="form-group">
            <label>Jenis Kelamin</label>
            <div class="radio">
              <label>
                <input type="radio" name="jenis_kelamin" id="input" value="pria">
                  Pria
              </label>
              <label style="margin-left: 10px">
                <input type="radio" name="jenis_kelamin" id="input" value="wanita">
                  Wanita
              </label>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="form-group">
          <label>Pilih Agama</label>
              <select name="agama" id="agama" class="form-control select2" style="width:100% !important">
                <option disabled selected>-Pilih Agama-</option>
                @foreach($agama as $data)
                  <option value="{{$data['id']}}">{{$data['agama']}}</option>
                @endforeach
              </select>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="form-group">
             <label>Pilih Pekawinan</label>
              <select name="perkawinan" id="perkawinan" class="form-control select2" style="width:100% !important">
                <option disabled selected>-Pilih Pekawinan-</option>
                @foreach($perkawinan as $data)
                  <option value="{{$data['id']}}">{{$data['perkawinan']}}</option>
                @endforeach
              </select>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="form-group">
            <label>Pilih Pendidikan</label>
            <select name="pendidikan" id="pendidikan" class="form-control select2" style="width:100% !important">
              <option disabled selected>-Pilih Pendidikan-</option>
              @foreach($pendidikan as $data)
                <option value="{{$data['id']}}">{{$data['pendidikan']}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="form-group">
            <label>Pilih Pekerjaan</label>
            <select name="pekerjaan" id="pekerjaan" class="form-control select2" style="width:100% !important">
              <option disabled selected>-Pilih Pekerjaan-</option>
              @foreach($pekerjaan as $data)
                <option value="{{$data['id']}}">{{$data['pekerjaan']}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="form-group">
            <label>Golongan Darah</label>
            <select name="golongandarah" id="golongandarah" class="form-control select2" style="width:100% !important">
              <option disabled selected>-Pilih Golongan Darah-</option>
              @foreach($golongandarah as $data)
                <option value="{{$data['id']}}">{{$data['golongandarah']}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <div class="form-group">
            <label>Aktivitas Disukai</label>
            
            <input type="hobi" name="hobi" class="form-control" required="">
            
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <div class="form-group">
            <label>No. Telp</label>
            <input type="text" name="telp" class="form-control" required="">
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label>Nama Kantor</label>
            <input type="text" name="namakantor" class="form-control">
          </div>
        </div>
        
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label>Alamat Kantor</label>
            <textarea class="form-control" name="alamatkantor" required=""></textarea>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label>Pilih Jaminan</label>
            <select name="jaminan" id="jaminan" class="form-control select2" style="width:100% !important">
              <option disabled selected>-Pilih Jaminan-</option>
              @foreach($penjamin as $data)
                <option value="{{$data['id']}}">{{$data['penjamin']}}</option>
              @endforeach
            </select>
          </div>
        </div>

      
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label>Catatan/Keterangan</label>
            <textarea class="form-control" name="catatan" required=""></textarea>
          </div>
        </div>

        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <label>Penanggung Jawab Pasien</label>
            </div>  
            <div class="panel-body">

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama_penanggung_jawab" class="form-control" required="">
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label>Hubungan Keluarga</label>
                  <select name="hubungan_keluarga" id="hubungan_keluarga" class="form-control select2" style="width:100% !important">
                    <option selected value="">-Pilih Hubungan-</option>
                    <option value="orangtua">Orangtua</option>
                    <option value="suami">Suami</option>
                    <option value="istri">Istri</option>
                    <option value="kakak">Kakak</option>
                    <option value="adik">Adik</option>
                  </select>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label>Alamat</label>
                  <textarea name="alamat_penanggung_jawab" class="form-control" required=""></textarea>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                  <label>Pilih Pekerjaan</label>
                  <select name="pekerjaan_penanggun_jawab" id="pekerjaan_penanggun_jawab" class="form-control select2" style="width:100% !important">
                    <option disabled selected>-Pilih Pekerjaan-</option>
                    @foreach($pekerjaan as $data)
                      <option value="{{$data['id']}}">{{$data['pekerjaan']}}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                  <label>No. Telp / HP</label>
                  <input type="text" name="telp_penanggung_jawab" class="form-control" required="">
                </div>
              </div>



            </div>
          </div>

        </div>

        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <label>Pengirim Pasien</label>
            </div>  
            <div class="panel-body">

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama_pengirim" class="form-control" required="">
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label>Instansi</label>
                  <select name="instansi_pengirim" id="instansi_pengirim" class="form-control select2" style="width:100% !important">
                    <option selected value="">-Pilih Instansi-</option>
                    <option value="pemerintahan">Pemerintahan</option>
                    <option value="swasta">Swasta</option>
                    <option value="lainnya">Lainnya</option>
                  </select>
                </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="form-group">
                  <label>Diagnosa</label>
                  <select name="diagnosa_pengirim" id="diagnosa_pengirim" class="form-control select2" style="width:100% !important">
                    <option selected value="">-Pilih Diagnosa-</option>
                    <option value="batuk">Batuk</option>
                    <option value="pilek">Pilek</option>
                    <option value="meriang">Meriang</option>
                  </select>
                </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="form-group">
                  <label>Tanggal Kedatangan</label>
                  <div class="input-group">
                    <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                    <input type="text" name="tgl_datang" class="form-control datepicker">
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="col-lg-12">
          <button type="submit" class="btn btn-block btn-primary btn-lg btn-flat">Simpan <i class="fa fa-save"></i></button>
        </div>

      </div>
        
      </form>
    </div>
  </div>

  <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
    <div class="x_panel">
      <div class="x_title">
        <h2>Daftar Antrian <a href="#" data-toggle="tooltip" data-placement="top" title="Tekan F5 untuk mendapatkan data terbaru"><i class="fa fa-question-circle"></i></a></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li data-count="{{ count($pasien) }}" style="margin-right: 5px;padding-top: 5px" id="count"><span class="badge" style="background: #448aff;color: #ffffff">{{ count($pasien) }}</span></li>
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <div class="">
          <ul class="to_do" id="daftar-antri">
            @if($pasien)
            <?php $no = 1; ?> @foreach($pasien as $data)
            <li>
              <p>{{$no++}}. {{ $data->nama }} ({{ $data->created_at->diffForHumans() }})<a href="#!" class="btn btn-danger btn-xs pull-right btn-hapus"
                  data-toggle="tooltip" title="Hapus Data" data-id="{{ $data['id'] }}"><i class="fa fa-trash"></i></a></p>
            </li>
            @endforeach @else
            <p style="text-align: center">Antrian Kosong</p>
            @endif
          </ul>
        </div>
      </div>
    </div>
  </div>

<!-- End to do list -->
<div class="clearfix"> </div>
@endsection @section('customJs')
<script type="text/javascript">
 

$(document).ready(function(){
  // $("#loading").hide(); // Sembunyikan loadingnya
  
    $("#btn-search").click(function(){ // Ketika user mengklik tombol Cari
        search(); // Panggil function search
    });
    
    $("#nis").keyup(function(){ // Ketika user menekan tombol di keyboard
    if(event.keyCode == 13){ // Jika user menekan tombol ENTER
      search(); // Panggil function search
    }
  });
});

//----------------------------------------------------
  $(document).ready(function () {
    $('#frm-pasien').on('submit', function () {
      toastr.success('Success !', 'Data berhasil di simpan !');
    });

    $('#pelayanan_dokter').on('change', function () {
      $('#pelayanan_dokter option:selected').each(function () {
        var spesialis = $(this).data('spesialis');
        $('#spesialis').val(spesialis);
      });
    });

    $('.btn-hapus').on('click', function (e) {
      var id = $(this).data('id');
      $.confirm({
        icon: 'fa fa-warning',
        title: 'Alert !',
        content: 'Apakah anda ingin menghapus data ini ?',
        type: 'red',
        typeAnimated: true,
        buttons: {
          confirm: function () {
            $.get("{{ route('getHapusPasien') }}", {
              id: id
            }, function (data) {
              toastr.success('Success !', 'Data berhasil di hapus');
              location.reload();
            });
          },
          cancel: function () {},
        }
      });
    });
  });
</script>

@endsection