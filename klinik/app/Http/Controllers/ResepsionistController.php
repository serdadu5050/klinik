<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pasien;
use App\Dokter;
use App\Pekerjaan;
use App\Perkawinan;
use App\Pendidikan;
use App\Agama;
use App\Penjamin;
use App\GolonganDarah;
use App\Penanggungjawab;
use App\Pengirim;
use App\Pasiendaftar;
use Excel;
use PDF;

class ResepsionistController extends Controller
{
    public function __construct() {
    	  $this->middleware('resepsionist');
    }
 
    public function index() {
            $total = Pasien::get()->toArray();
            $pasien = Pasiendaftar::whereDate('created_at', '=', date('Y-m-d'))->where('status', '=', 'antri')->get();
            $bulan = Pasiendaftar::whereMonth('created_at', '=', date('m'))->whereYear('created_at', '=', date('Y'))->get()->toArray();
        
           
            $dokter = Dokter::with('spesialis')->get()->toArray();        
            $id = Pasien::select('id')->get()->last();
            $pekerjaan = pekerjaan::get()->toArray();
            $perkawinan = perkawinan::get()->toArray();
            $pendidikan = pendidikan::get()->toArray();
            $golongandarah = golongandarah::get()->toArray();
            $penjamin = penjamin::get()->toArray();
            $agama = agama::get()->toArray();
            if ($id == null) {
                $id = 1;
            }
            $id  = substr($id['id'], 4);
            $id = (int) $id;
            $id += 1;
            $id  = "PS" . str_pad($id, 4, "0", STR_PAD_LEFT);
    	return view('resepsionist.index', [
                                'pasien' => $pasien, 
                                'id' => $id, 
                                'total' => $total,
                                'bulan' => $bulan,
                                'dokter' => $dokter,
                                'pekerjaan'=> $pekerjaan,
                                'perkawinan'=> $perkawinan,
                                'pendidikan'=> $pendidikan,
                                'golongandarah'=> $golongandarah,
                                'penjamin'=> $penjamin,
                                'agama'=> $agama
                                ]);
    }

    public function getPasien() {
        $HariIni = Pasiendaftar::whereDate('created_at', '=', date('Y-m-d'))->where('status', '=', 'antri')->get();
        $bulan = Pasiendaftar::whereMonth('created_at', '=', date('m'))->whereYear('created_at', '=', date('Y'))->get()->toArray();
        // $pasien = Pasien::where('status', '=', 'antri')->orderBy('created_at', 'desc')->groupBy('nama')->get()->toArray();
        $pasien = Pasien::orderBy('created_at', 'desc')->groupBy('nama')->get()->toArray();
        $dokter = Dokter::with('spesialis')->get()->toArray();
        // dd($dokter); 
    	return view('resepsionist.pasien.index', ['pasien'=> $pasien, 'bulan' => $bulan, 'HariIni' => $HariIni, 'dokter' => $dokter]);
    }

    public function getPasienDaftar() {
        $HariIni = Pasien::whereDate('created_at', '=', date('Y-m-d'))->where('status', '=', 'antri')->get();
        $bulan = Pasien::whereMonth('created_at', '=', date('m'))->whereYear('created_at', '=', date('Y'))->get()->toArray();
        // $pasien = Pasien::where('status', '=', 'antri')->orderBy('created_at', 'desc')->groupBy('nama')->get()->toArray();
        $pasien = Pasien::orderBy('created_at', 'desc')->groupBy('nama')->get()->toArray();
        $dokter = Dokter::with('spesialis')->get()->toArray();
        // dd($dokter); 
    	return view('resepsionist.pasien.index', ['pasien'=> $pasien, 'bulan' => $bulan, 'HariIni' => $HariIni, 'dokter' => $dokter]);
    }



    


    public function postPendaftaranPasien(Request $request) {
            // dd($request->all());
            $data = Pasien::create($request->all());
           
            $penanggungjawab = penanggungjawab::create([
                    'idpasien'=> $request->id,
                    'nama_penanggung_jawab'=> $request->nama_penanggung_jawab,
                    'hubungan_keluarga'=> $request->hubungan_keluarga,
                    'alamat_penanggung_jawab'=> $request->alamat_penanggung_jawab,
                    'tempatlahir_penanggung_jawab'=> $request->tempatlahir_penanggung_jawab,
                    'tgl_lahir_penanggung_jawab'=> $request->tgl_lahir_penanggung_jawab,
                    'pekerjaan_penanggun_jawab'=> $request->pekerjaan_penanggun_jawab,
                    'telp_penanggung_jawab'=> $request->telp_penanggung_jawab
                ]);

                $pengirim = pengirim::create([
                    'idpasien'=> $request->id,
                    'nama_pengirim'=> $request->nama_pengirim,
                    'instansi_pengirim'=> $request->instansi_pengirim,
                    'diagnosa_pengirim'=> $request->diagnosa_pengirim,
                    'tgl_datang'=> $request->tgl_datang
                ]);    
                
                $nopendaftaran = pasiendaftar::select('nopendaftaran')->get()->last();
                if ($nopendaftaran == null) {
                    $nopendaftaran = 1;
                }
                $nopendaftaran  = substr($nopendaftaran['nopendaftaran'], 5);
                $nopendaftaran = (int) $nopendaftaran;
                $nopendaftaran += 1;
                $nopendaftaran  = "REG" . str_pad($nopendaftaran, 5, "0", STR_PAD_LEFT);
                $pasiendaftar = pasiendaftar::create([
                    'nopendaftaran'=> $nopendaftaran,
                    'idpasien'=> $request->id,
                    'nama'=>$request->nama,
                    'status' => 'antri'
                   
                ]);
        
                return redirect()->back();    

    }
    
    public function postPasienTerdaftar(Request $request) {
        if($request->ajax()){
            
            $nopendaftaran = pasiendaftar::select('nopendaftaran')->get()->last();
            if ($nopendaftaran == null) {
                $nopendaftaran = 1;
            }
            $nopendaftaran  = substr($nopendaftaran['nopendaftaran'], 5);
            $nopendaftaran = (int) $nopendaftaran;
            $nopendaftaran += 1;
            $nopendaftaran  = "REG" . str_pad($nopendaftaran, 5, "0", STR_PAD_LEFT);
            $pasiendaftar = pasiendaftar::create([
                'nopendaftaran'=> $nopendaftaran,
                'idpasien'=> $request->id,
                'nama'=>$request->nama,
                'status' => 'antri'
               
            ]);
            return redirect()->back();    
           
           

        }
    }
 
  

    public function getHapusPasien(Request $request) {
        if ($request->ajax()) {
            $data = Pasien::find($request->id)->delete();
            return response()->json($data);
        }
    }

    public function postUpdatePasien(Request $request) {
        if ($request->ajax()) {
            $data = Pasien::find($request->id)->update($request->all());
            return response()->json($data);
        }
    }

    public function exportExcelPasien(Request $request, $type) {
         Excel::create('Data Pasien ' .  $request->bulan .'-' .$request->tahun, function ($excel) use ($request){
                $excel->sheet('Data Pasien ' .  $request->bulan .'-' .$request->tahun, function ($sheet) use ($request){
                    $bulan = $request->bulan;
                    $tahun = $request->tahun;
                    $arr = array();
                    $barang = Pasien::whereMonth('created_at', $bulan)->whereYear('created_at', $tahun)->get()->toArray();
                    foreach ($barang as $data) {
                        $data_arr = array(
                            $data['id'],
                            $data['nama'],
                            $data['jenis_kelamin'],
                            $data['tgl_lahir'],
                            $data['pekerjaan'],
                            $data['telp'],
                            $data['alamat'],
                        );
                        array_push($arr, $data_arr);
                    }
                    $sheet->fromArray($arr, null, 'A1', false, false)->prependRow(array(
                        'ID',
                        'Nama Pasien',
                        'Jenis Kelamin',
                        'Tgl. Lahir',
                        'Pekerjaan',
                        'No. Telp',
                        'Alamat',
                    ));
                });
            })->download($type);
    }

     public function exportPDFPasien(Request $request) {
         $bulan = $request->bulan;
         $tahun = $request->tahun;
         $pasien = Pasien::whereMonth('created_at', $bulan)
                            ->whereYear('created_at', $tahun)
                            ->get()->toArray();
        // $pdf = PDF::render();
        return view('resepsionist.pasien.pdf', ['bulan' => $bulan, 'tahun' => $tahun, 'pasien' => $pasien]);
    }

    
}
