<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasiensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasiens', function (Blueprint $table) {
            $table->char('id', 10);
            $table->primary('id');
            $table->string('nama', 100);
            $table->enum('jenis_kelamin', ['pria', 'wanita']);
            $table->text('alamat');
            $table->text('tempatlahir');
            $table->date('tgl_lahir');
            $table->string('telp', 13);
            $table->string('nohp', 13);
            $table->enum('perkawinan', ['kawin', 'belumkawin', 'duda', 'janda']);
            $table->string('agama', 40);
            $table->string('pendidikan', 40);
            $table->string('noktp', 40);
            $table->string('jaminan', 40);
            $table->string('hobi', 40);
            $table->string('pekerjaan', 40);
            $table->string('namakantor', 40);
            $table->string('golongandarah', 40);
            $table->string('catatan', 40);
            $table->enum('status', ['antri', 'periksa', 'obat', 'selesai']);
            $table->string('layanan_dokter', 15);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasiens');
    }
}
